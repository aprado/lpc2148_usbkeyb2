#include"LPC214X.h"
#include"key.h"
#include "type.h"

BYTE keyCurrent=0;
BYTE keyenable=1;
BYTE keyold;

//只允许一个按键 
void IRQ_Timer0()__irq
{
     int a=0;
	 BYTE key=0;
	 keyCurrent=0;
     a=IO0PIN;
	 a=a&0X0000FC00;
	 a=a>>10;
	 if(a==0)
	 {
	     key=0;
	 }
	 else
	 {
	     switch(a)
		 {
	        case  0x3e: key=3;	   //对应于BP8
		    break;
		    case  0x3d: key=2;	   //对应于BP7
		    break;
		    case  0x3b: key=1;	   //对应于BP6
		    break;
		    case  0x37: key=6;	   //对应于BP5
		    break;
		    case  0x2f: key=5;	   //对应于BP4
		    break;
		    case  0x1f: key=4;	   //对应于BP3
		    break;
		    default: key=0;  
		    break;
		  }
	 }
	 if(keyenable==1)
	 {
	      switch(key)
		  {
		     case 1: keyCurrent=0x28;
		     break;
		     case 2: keyCurrent=0x52;
		     break;
		     case 3: keyCurrent=0x02;
		     break;
		     case 4: keyCurrent=0x50;
		     break;
		     case 5: keyCurrent=0x51;
		     break;
		     case 6: keyCurrent=0x4f;
		     break;
		     case 0: keyCurrent=0;
		     break;
			 default:	keyCurrent=0;
			 break;
		  }
	 }
	 T0IR=0X01;
	 VICVectAddr=0X00;
}

INIT_TIME()
{
    IO0DIR=0;
    VICIntSelect=0x00;
	VICIntEnable=0X10;
    VICVectCntl1 = 0x20 | 4;
	VICVectAddr1=(unsigned int)IRQ_Timer0;
    T0TC=0;
	T0PR=0;
	T0MCR=0X03;
	T0MR0=12000000/100;	    //10MS 
	T0TCR=0X01;
}

